#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "kcont.h"

/* using kcontainer-lib with custom tyoe example */

typedef struct test_struct {
	int i;
	char str[512];
} foo_t;

DEF_CONT(foo_t)

int main ()
{
	struct foo_t_cont_t *foo_cont;
	foo_t foo;

	foo_cont = init_foo_t_cont();

	foo.i = 168;
	strcpy(foo.str, "foo string");

	append_foo_t(&foo_cont, foo);

	printf("%i %s\n", el(foo_cont, 0).i, el(foo_cont, 0).str);

	free(foo_cont);

	return 0;
}
