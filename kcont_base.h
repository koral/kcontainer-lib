#ifndef _pmc_kcontbase_h_included_
#define _pmc_kcontbase_h_included_

#include <stddef.h>

#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

struct cont_t {
	size_t len;
	size_t max_len;
};

extern struct cont_t *init_cont(size_t sizeof_elem);

extern void resize_cont(struct cont_t **coll_ref, const size_t sizeof_elem);

extern void shrink_cont(struct cont_t **coll_ref, const size_t sizeof_elem);

#endif
