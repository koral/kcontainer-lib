kcontainer-lib
==============

kcontainer-lib is a library that implements a simple containers for every kind of data type.

kcontainer-lib is:
* fast: provide direct array access to the member of the container.
* flexible: you can define containers and handling functions to every kind of type with in one line.
* serialization friendly: all the containers created and selfcontained and already serialized.

Author
----
Andrea Corallo

andrea_corallo@yahoo.it

http://www.kkoral.blogspot.com
---

Example:

```C
#include <stdlib.h>
#include <stdio.h>
#include "kcont.h"

DEF_CONT(int)

int main()
{
	int i;
	struct int_cont_t *int_cont;

	int_cont = init_int_cont();

	append_int(&int_cont, 3);
	append_int(&int_cont, 2);
	append_int(&int_cont, 78);

	el(int_cont, 1) = 2 * el(int_cont, 1);

	for(i = 0; i < 3; i++)
		printf("%d\n", el(int_cont, i));

	shrink_int(&int_cont);

	free(int_cont);

	return 0;
}
```

Example on how to expand kcontainer-lib to support custom types:

```C
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "kcont.h"

typedef struct test_struct {
	int i;
	char str[512];
} foo_t;

DEF_CONT(foo_t);

int main()
{
	struct foo_t_cont_t *foo_cont;
	foo_t foo;

	foo_cont = init_foo_t_cont();

	foo.i = 168;
	strcpy(foo.str, "foo string");

	append_foo_t(&foo_cont, foo);

	printf("%i %s\n", el(foo_cont, 0).i, el(foo_cont, 0).str);

	free(foo_cont);

	return 0;
}

```
---

Interface:

**DEF_CONT(type)**

Define the internal container struct and the interface for type type.

**void append_TYPE(TYPE_cont_t *container, TYPE element)**

Append element to container (substitute TYPE with the current type you are using).

**void shrink_TYPE(TYPE_cont_t *container, TYPE element)**

Shrink the container to the minimum size.

**TYPE el(TYPE_cont_t container, size_t index)**

The element number index of the container.

**size_t n_elem(TYPE_cont_t container)**

The number of elements actually contained.

**size_t max_elem(TYPE_cont_t container)**

The total maximum number of elements storable before the next reallocation.

**size_t sizeof_cont(TYPE_cont_t container)**

The actual size in bytes of the container.
