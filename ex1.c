#include <stdlib.h>
#include <stdio.h>
#include "kcont.h"

/* using kcontainer-lib with int example */

DEF_CONT(int)

int main ()
{
	int i;
	struct int_cont_t *int_cont;

	int_cont = init_int_cont();

	for(i = 0; i < (2 << 10) + 1; i++)
		append_int(&int_cont, i);

	for(i = 0; i < (2 << 10) + 1; i++)
		el(int_cont, i) = 2 * el(int_cont, i);

	for(i = 0; i < 10; i++)
		printf("%d\n", el(int_cont, i));

	printf("the actual size of the container in bytes before the shrink is %ld bytes.\n",
		sizeof_cont(int_cont));

	shrink_int(&int_cont);

	printf("the actual size of the container in bytes after the shrink is %ld bytes.\n",
		sizeof_cont(int_cont));

	free(int_cont);

	return 0;
}
