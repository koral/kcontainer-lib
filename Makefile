HEADERS = kcont_base.h kcont.h
CC = gcc
CFLAGS = -Wall -Wextra -pedantic-errors -g

%.o: %.c $(HEADERS)
	$(CC) -c -o $@ $< $(CFLAGS)

default: ex1 ex2

ex1: kcont_base.o ex1.o
	$(CC) $(CFLAGS) ex1.o kcont_base.o -o ex1

ex2: kcont_base.o ex2.o
	$(CC) $(CFLAGS) ex2.o kcont_base.o -o ex2

clean:
	-rm *.o ex1 ex2
