#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "kcont_base.h"

#define INIT_SIZE (2 << 8)

static void *xrealloc(void *p, size_t size);

extern struct cont_t *init_cont(size_t sizeof_elem)
{
	struct cont_t *coll = NULL;

	resize_cont(&coll, sizeof_elem);

	return coll;
}

extern void resize_cont(struct cont_t **coll_ref, const size_t sizeof_elem)
{
	size_t new_byte_size, old_byte_size, new_el_len;
	struct cont_t *coll = *coll_ref;

	assert(sizeof_elem);

	if (unlikely(!coll)) {
		new_byte_size = INIT_SIZE * sizeof_elem +
			sizeof(struct cont_t);
		old_byte_size = 0;
		new_el_len = INIT_SIZE;
	}
	else {
		old_byte_size = coll->max_len * sizeof_elem +
			sizeof(struct cont_t);
		new_byte_size = 2 * (coll->max_len * sizeof_elem) +
			sizeof(struct cont_t);
		new_el_len = 2 * coll->max_len;
	}

	coll = xrealloc(coll, new_byte_size);

	memset((char *)coll + old_byte_size, 0, new_byte_size - old_byte_size);
	coll->max_len = new_el_len;
	*coll_ref = coll;

	return;
}

extern void shrink_cont(struct cont_t **coll_ref, const size_t sizeof_elem)
{
	size_t new_byte_size;
	struct cont_t *coll = *coll_ref;

	assert(sizeof_elem);

	new_byte_size = (coll->len * sizeof_elem) +
		sizeof(struct cont_t);

	coll = xrealloc(coll, new_byte_size);
	coll->max_len = coll->len;
	*coll_ref = coll;

	return;
}


static void *xrealloc(void *p, size_t size)
{
	p = realloc(p, size);
	if (unlikely(!p)) {
		fprintf(stderr, "Out of memory");
		exit (EXIT_FAILURE);
	}

	return p;
}
